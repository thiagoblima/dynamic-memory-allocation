//
// Created by Samsung on 10/08/2018.
//

#include <iostream>
#include <istream>
#include <iomanip>

#ifndef MALLOCINIT_DATA_H
#define MALLOCINIT_DATA_H

struct data {

protected:
    enum gender : char {
        male = 'm',
        female ='f',
        standard = 's'
    }genderp;

    enum taxes : int {
        basic = 1,
        intermediate = 2,
        advanced = 3,
        standardt = 4
    }taxesp;

private:
    int id;
    std::string name;
    double tax;

public:
    int &getId();
    double &baseTax();
    double &interTax();
    double &advanceTax();
    std::string &getName();
    data::gender &getGender();
    data::taxes &getTaxes();
    data() = default;

}datap;

int &data::getId(){
    return data::id;
}

double &data::baseTax(){
    return data::tax += 2.4;
}

double &data::interTax(){
    return data::tax += 2.8;
}

double &data::advanceTax(){
    return data::tax += 3.2;
}

std::string &data::getName(){
    return data::name;
}

data::gender &data::getGender(){
    char g = genderp;
    std::cout << "Enter your orientation: ";
    std::cin >> g;
    switch(g){
        case data::gender::male              : std::cout << "Male orientation selected\n"; break;
        case data::gender::female            : std::cout << "female orientation selected\n"; break;
        default: case data::gender::standard : std::cout << "No orientation provided\n";
    }
}

data::taxes &data::getTaxes(){
    int t = taxesp;
    std::cout << "Enter the plan: ";
    std::cin >> t;
    switch(t){
        case data::taxes::basic              : std::cout << t << " - Basic plan \n"        << "Tax over the total price: " << std::setprecision(3) << (6.60 / data::baseTax()) << "\n"; break;
        case data::taxes::intermediate       : std::cout << t << " - Intermediate plan \n" << "Tax over the total price: " << std::setprecision(3) << (12.8 / data::interTax()) << "\n"; break;
        case data::taxes::advanced           : std::cout << t << " - Advanced plan \n"     << "Tax over the total price: " << std::setprecision(3) << (24.6 / data::advanceTax()) << "\n"; break;
        default: case data::taxes::standardt : std::cout << t << std::endl << "Standard tax is selected (U$6.2)\n";
    }
}

#endif //MALLOCINIT_DATA_H
