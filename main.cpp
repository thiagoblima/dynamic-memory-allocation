#include "data.h"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

void message_print(data *p){

    string str;

    cout << "Welcome to dynamic memory allocation, enter your personal data:" << endl;

    cout << "Enter your name:";
    getline(cin, p->getName());

    cout << "Enter an id:";
    getline(cin, str);
    (stringstream) str >> p->getId();

    p->getGender();
    p->getTaxes();

    cout << setfill('*');
    cout << "Created Name:   " <<  setw(+5) << setiosflags(ios::left) <<  p->getName() << endl;
    cout << "Created Id:     " <<  setw(+3) << setiosflags(ios::right) <<  p->getId() << endl;

    delete p;

}

int main()
{
    message_print(&datap);
    return 0;
}
